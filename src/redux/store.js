import currentEvent from './reducer/current-event.reducer';
import listEvents from './reducer/list-events.reducer';
import user from './reducer/user.reducer';
import socket from './reducer/socket.reducer';
import isOpenModalCreateEvent from './reducer/is-open-modal-create-event.reducer';
import isOpenNavRightProfile from './reducer/is-open-nav-right-profile.reducer';
import isOpenModalUpdateProfile from './reducer/is-open-modal-update-profile.reducer';
import isOpenModalMyEvents from './reducer/is-open-modal-my-events.reducer';
import isOpenAppShow from './reducer/is-open-app-show.reducer';
import myEvents from './reducer/my-events.reducer';

import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

// reducer
var reducer = combineReducers({
  user,
  currentEvent,
  listEvents,
  myEvents,
  socket,
  isOpenModalCreateEvent,
  isOpenNavRightProfile,
  isOpenModalUpdateProfile,
  isOpenModalMyEvents,
  isOpenAppShow
});

// store
const store = createStore(reducer, applyMiddleware(thunk));
store.subscribe(() => {
    console.log("STORE UPDATED");
    console.log(store.getState());
});

export default store;
