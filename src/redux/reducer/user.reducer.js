let userReducer = (state = null, action) => {
    switch (action.type) {
        case 'CHANGE_USER':
            state = action.user;
            return state;
        case 'CLEAR_USER':
            state = null;
            return state;
        case 'HANDLE_INCREASE_ON_USER':
          if(action.obj.type === 'going') {
            state.going.push(action.obj.going);
          } else {
            state.interested.push(action.obj.interested);
          }
          return {...state};
        case 'HANDLE_DECREASE_ON_USER':
          var i = 0;
          if(action.obj.type === 'going') {
            for(i; i < state.going.length; i++) {
              if(String(state.going[i].id) === String(action.obj.event_id)) {
                state.going.splice(i, 1);
                break;
              }
            }
          } else {
            for(i; i < state.interested.length; i++) {
              if(String(state.interested[i].id) === String(action.obj.event_id)) {
                state.interested.splice(i, 1);
                break;
              }
            }
          }
          return {...state};
        default:
            return state;
    }
}

export default userReducer;
