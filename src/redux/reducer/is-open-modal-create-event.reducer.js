const default_state = false;

let isOpenModalCreateEvent = (state = default_state, action) => {
    switch (action.type) {
        case 'TOGGLE_MODAL_CREATE_EVENT':
            return state = action.isOpenModalCreateEvent;
        default:
            return state;
    }
}

export default isOpenModalCreateEvent;
