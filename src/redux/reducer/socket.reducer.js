import io from 'socket.io-client';
import CONFIG from '../../config';

let socket = io(CONFIG.URL_SOCKET);

let currentEventReducer = (state = socket, action) => {
    return state;
}

export default currentEventReducer;
