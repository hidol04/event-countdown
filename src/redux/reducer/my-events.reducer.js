let myEvents = (state = null, action) => {
    switch (action.type) {
        case 'GET_MY_EVENTS':
            state = action.events;
            return state;
        case 'ADD_MY_EVENTS':
            state.push(action.evenT);
            return state;
        default:
            return state;
    }
}

export default myEvents;
