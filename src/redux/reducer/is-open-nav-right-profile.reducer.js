const default_state = false;

let isOpenNavRightProfile = (state = default_state, action) => {
    switch (action.type) {
        case 'OPEN_NAV_RIGHT_PROFILE':
            return state = action.isOpenNavRightProfile;
        default:
            return state;
    }
}

export default isOpenNavRightProfile;
