let userReducer = (state = null, action) => {
    switch (action.type) {
        case 'CHANGE_USER':
            state = action.user;
            return {...state};
        default:
            return state;
    }
}

export default userReducer;
