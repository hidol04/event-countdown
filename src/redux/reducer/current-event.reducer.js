let currentEventReducer = (state = null, action) => {
    switch (action.type) {
        case 'CHANGE_EVENT':
            state = action.event;
            return {...state};
        case 'UPDATE_MESSAGES':
          state.messages.push(action.message);
          return {...state};
        case 'HANDLE_INCREASE':
          if(action.obj.type === 'going') {
            state.going.push(action.obj.going);
          } else {
            state.interested.push(action.obj.interested);
          }
          return {...state};
        case 'HANDLE_DECREASE':
          if(action.obj.type === 'going') {
            state.going = state.going.filter(function(obj) {
              return obj === action.obj.user_id;
            });
          } else {
            state.interested = state.interested.filter(function(obj) {
              return obj === action.obj.user_id;
            });
          }
          return {...state};
        case 'UPLOAD_PHOTOS':
          state.photos.push(action.photos);
          return {...state};
        default:
            return state;
    }
}

export default currentEventReducer;
