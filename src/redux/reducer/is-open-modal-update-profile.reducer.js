const default_state = false;

let isOpenModalUpdateProfile = (state = default_state, action) => {
    switch (action.type) {
        case 'TOGGLE_MODAL_UPDATE_PROFILE':
            return state = action.isOpenModalUpdateProfile;
        default:
            return state;
    }
}

export default isOpenModalUpdateProfile;
