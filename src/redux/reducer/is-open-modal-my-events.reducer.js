const default_state = false;

let isOpenModalMyEvents = (state = default_state, action) => {
    switch (action.type) {
        case 'TOGGLE_MODAL_MY_EVENTS':
            return state = action.isOpen;
        default:
            return state;
    }
}

export default isOpenModalMyEvents;
