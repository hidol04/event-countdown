const default_state = false;

let isOpenAppShow = (state = default_state, action) => {
    switch (action.type) {
        case 'TOGGLE_APP_SHOW':
            return state = action.isOpenAppShow;
        default:
            return state;
    }
}

export default isOpenAppShow;
