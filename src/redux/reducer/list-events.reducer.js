let listEvents = (state = null, action) => {
    switch (action.type) {
        case 'UPDATE_LIST_EVENTS':
            state = action.events;
            return state;
        default:
            return state;
    }
}

export default listEvents;
