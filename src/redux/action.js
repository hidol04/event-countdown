export function changeUser(user) {
  return {
      type: "CHANGE_USER",
      user
  }
}

export function clearUser() {
    return {
        type: "CLEAR_USER"
    }
}

export function changeCurrentEvent(event) {
    return {
        type: "CHANGE_EVENT",
        event
    }
}

export function uploadPhotos(photos) {
    return {
        type: "UPLOAD_PHOTOS",
        photos
    }
}

export function updateListEvents(events) {
    return {
        type: "UPDATE_LIST_EVENTS",
        events
    }
}

export function getMyEvents(events) {
    return {
        type: "GET_MY_EVENTS",
        events
    }
}
export function addMyEvents(evenT) {
    return {
        type: "ADD_MY_EVENTS",
        evenT
    }
}

export function handleIncreaseOnEvent(obj) {
    return {
        type: "HANDLE_INCREASE",
        obj
    }
}

export function handleDecreaseOnEvent(obj) {
    return {
        type: "HANDLE_DECREASE",
        obj
    }
}

export function handleIncreaseOnUser(obj) {
    return {
        type: "HANDLE_INCREASE_ON_USER",
        obj
    }
}

export function handleDecreaseOnUser(obj) {
    return {
        type: "HANDLE_DECREASE_ON_USER",
        obj
    }
}

export function updateMessages(message) {
    return {
        type: "UPDATE_MESSAGES",
        message
    }
}

export function isOpenCreateEventModal(isOpen) {
    return {
        type: "TOGGLE_CREATE_EVENT_MODAL",
        isOpenCreateEventModal: isOpen
    }
}

export function toggleNavRightProfile(isOpen) {
    return {
        type: "OPEN_NAV_RIGHT_PROFILE",
        isOpenNavRightProfile: isOpen
    }
}

export function toggleModalUpdateProfile(isOpen) {
    return {
        type: "TOGGLE_MODAL_UPDATE_PROFILE",
        isOpenModalUpdateProfile: isOpen
    }
}

export function toggleModalCreateEvent(isOpen) {
    return {
        type: "TOGGLE_MODAL_CREATE_EVENT",
        isOpenModalCreateEvent: isOpen
    }
}

export function toggleModalMyEvents(isOpen) {
    return {
        type: "TOGGLE_MODAL_MY_EVENTS",
        isOpen: isOpen
    }
}

export function toggleAppShow(isOpen) {
    return {
        type: "TOGGLE_APP_SHOW",
        isOpenAppShow: isOpen
    }
}
