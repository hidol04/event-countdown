import React, {
  Component
} from 'react';
import { connect } from 'react-redux';
import { updateListEvents } from '../../redux/action';
import EventSummary from '../Event/event-summary.component';
import { getAllEvents } from '../../blocks/api/event.api.js';
import AppWidget from './app-widget.component';

class AppHome extends Component {
  componentWillMount() {
    getAllEvents().then((data) => {
      var { dispatch } = this.props;
      dispatch(updateListEvents(data.body));
    });
  }
  render() {
    if(this.props.store.listEvents === null) return null;

    return (
      <div className="AppHome">
        <div className="Main">
          {this.props.store.listEvents.map(function(event, i) {
            return (
              <EventSummary key={i} data={event}></EventSummary>
            )
          })}
        </div>

        <AppWidget />
      </div>
    )
  }
}

export default connect((state) => {
  return {
    store: {
      listEvents: state.listEvents,
    }
  }
})(AppHome);
