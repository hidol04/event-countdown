import React, {
  Component
} from 'react';
import Paper from 'material-ui/Paper';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

class AppWidget extends Component {
  render() {
    return (
      <div className="AppWidget">
        <Paper className="AppWidget-Paper" zDepth={1} rounded={false} transitionEnabled={false}>
          <Table>
            <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
              <TableRow>
                <TableHeaderColumn>New feature</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow>
                <TableRowColumn>- [08/07/2015]: Hashtag for each events</TableRowColumn>
              </TableRow>
              <TableRow>
                <TableRowColumn>- [08/07/2015]: Update cover of event</TableRowColumn>
              </TableRow>
              <TableRow>
                <TableRowColumn>- [08/07/2015]: Set image default for broken images</TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>
      </div>
    )
  }
}

export default AppWidget;
