import React, { Component } from 'react';
import { connect} from 'react-redux';
import { updateMessages } from '../../redux/action';
import EventDetail from '../Event/event-detail.component';
import AppShow from './app-show.component';
import NavRight from '../Nav/nav-right.component';

class AppDetail extends Component {
    constructor(props) {
      super(props);
      this.state = {
          isFirstTimeToJoinAnEvent: false,
          isReciveMessageExist: false
      };
    }
    componentDidMount() {
        this.props.store.socket.on('s send message', (message) => {
          var { dispatch } = this.props;
          dispatch(updateMessages(message));
        });
    }
    componentWillReceiveProps(nextProps) {
      if(this.props.store.user === null) return;

      let { currentEvent } = this.props.store;
      let { user } = this.props.store;
      let nextEvent = nextProps.store.currentEvent;
      let { socket } = nextProps.store;
      // Join an event after page loaded
      if (!this.state.isFirstTimeToJoinAnEvent && nextEvent) {
        socket.emit('join event', user._id, nextEvent._id);
        this.setState({ isFirstTimeToJoinAnEvent: !this.state.isFirstTimeToJoinAnEvent });
      } else {
        // Join an anoter event
        if (currentEvent && nextEvent)
          if (JSON.stringify(currentEvent._id) !== JSON.stringify(nextEvent._id)) {
            socket.emit('leave event', user._id, currentEvent._id);
            socket.emit('join event', user._id, nextEvent._id);
          }
      }
    }
    render() {
      if(this.props.store.currentEvent == null || this.props.store.user == null) return null;

      return (
        <div id="App" className="App">
          <EventDetail event={this.props.store.currentEvent} user={this.props.store.user}/>
          <NavRight />
          <AppShow />
        </div>
      )
    }
}

export default connect((state) => {
  return {
    store: {
      currentEvent: state.currentEvent,
      user: state.user,
      socket: state.socket
    }
  }
})(AppDetail);
