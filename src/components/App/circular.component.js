import React, {Component} from 'react';
import moment from 'moment';
import './circular.css';

class Circular extends Component {
    constructor(props) {
        super(props);
        this.setctx = this.setctx.bind(this);
        this.setTrack = this.setTrack.bind(this);
        this.setTime = this.setTime.bind(this);
    }

    render() {
      if(!this.props.isOpen) return null;

      requestAnimationFrame(this.setctx.bind(this));
      return (
        <div className="circle" id={this.props.type + "_" + this.props.id}>
            <canvas className={'canvas ' + this.props.type} width={this.props.size} height={this.props.size}></canvas>
            <div className="circle-text">

            </div>
        </div>
      );
    }

    clearctx (ctx) {
        ctx.clearRect(0, 0, 200, 200);
    }

    setTrack (ctx) {
      let radius = (this.props.size - ctx.lineWidth*2)/2 -14;
        ctx.strokeStyle = 'hsla(0, 0%, 15%, 0.45)';
        ctx.lineWidth = 4;
        ctx.lineJoin = 'round';
        ctx.shadowBlur = 1;
        ctx.beginPath();
        ctx.arc(90, 90, radius, 0, Math.PI*2);
        ctx.stroke();
        ctx.closePath();
    }

    setTime (ctx, until, now, total) {
        switch (this.props.type) {
            case 'milliseconds':
                ctx.strokeStyle = '#ffdc50';
                break;
            case 'seconds':
                ctx.strokeStyle = '#9cdc7e';
                break;
            case 'minutes':
                ctx.strokeStyle = '#378cff';
                break;
            case 'hours':
                ctx.strokeStyle = '#ff6565';
                break;
            case 'days':
                ctx.strokeStyle = '#d940e0';
                break;
            default : break;
        }
        let radius = (this.props.size - ctx.lineWidth*2)/2 - 14;
        ctx.lineJoin = 'round';
        ctx.shadowBlur = 1;
        ctx.lineWidth = 4;
        ctx.beginPath();
        ctx.arc(
            90,
            90,
            radius,
            Math.PI/-2,
            ( Math.PI * 2 ) * ( ( until - now % total ) / total ) + ( Math.PI / -2 ),
            false
        );
        ctx.stroke();
        ctx.closePath();
    }

    parseDate (time) {
        let current = new Date(time);
        return current.getSeconds();
    }

    setText (ctx, until) {
        ctx.font = "14px 'Ubuntu Condensed'";
        ctx.fillStyle = "#ffffff";
        ctx.textAlign = 'center';
        ctx.shadowColor = 'rgb(54, 57, 62)';
        ctx.shadowBlur = 1;
        ctx.fillText(until, 90, 90);
    }

    setctx() {
        let selector = document.querySelector('#' + this.props.type + '_' + this.props.id +' canvas');
        if (!selector) return;

        let ms = selector.getContext('2d'),
        grad = new Date(this.props.date_end),
        today = new Date(),
        duration = moment.duration(grad.getTime() - today.getTime());
        ms.imageSmoothingEnabled = false;

        this.clearctx(ms);
        this.setTrack(ms);
        switch (this.props.type) {
            case 'milliseconds':
                this.setTime(ms, grad.getMilliseconds() / 60, today.getMilliseconds() / 60, 1000 / 60);
                this.setText(ms, duration.milliseconds() + ' ms');
                break;
            case 'seconds':
                this.setTime(ms, grad.getSeconds() * 1000 + grad.getMilliseconds(), today.getSeconds() * 1000 + today.getMilliseconds(), 60 * 1000);
                this.setText(ms, duration.seconds() + ' seconds');
                break;
            case 'minutes':
                this.setTime(ms, grad.getMinutes() * 60 * 1000 + grad.getSeconds() * 1000 + grad.getMilliseconds(), today.getMinutes() * 60 * 1000 + today.getSeconds() * 1000 + today.getMilliseconds(), 60 * 60 * 1000);
                this.setText(ms, duration.minutes() + ' minutes');
                break;
            case 'hours':
                this.setTime(ms, grad.getHours(), today.getHours(), 24);
                this.setText(ms, duration.hours() + ' hours');
                break;
            case 'days':
                this.setTime(ms, grad.getHours(), today.getHours(), 24);
                this.setText(ms, Math.floor(duration.as('days')) + ' days');
                break;
            default : break;
        }
        requestAnimationFrame(this.setctx.bind(this));
    }

}

export default Circular
