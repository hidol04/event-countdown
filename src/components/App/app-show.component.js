import React, { Component } from 'react';
import { connect} from 'react-redux';
import { toggleAppShow } from '../../redux/action';
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import Circular from './circular.component';

class AppShow extends Component {
    constructor(props) {
        super(props);

        this.handleClose = this.handleClose.bind(this);
    }
    handleClose() {
      var { dispatch } = this.props;
      dispatch(toggleAppShow(false));
    }
    render() {
        if (this.props.store.currentEvent === null ||
          !this.props.store.isOpenAppShow) return null;

        return (
          <Drawer
            className="AppShow"
            openSecondary={false} width={'100%'}
            animated={false}
            iconStyleRight={{'margin-top': 0}}
            open={this.props.store.isOpenAppShow}>
            <div className="AppShow-Content">
                <div className="logo">
                    <img src="https://vignette4.wikia.nocookie.net/zelda/images/b/b0/E3_Logo.png/revision/latest?cb=20111002140101" alt="" />
                    <div className="title">
                        Waiting for E3
                    </div>
                    <div className="preloader">
                        <div className="ball-scale-ripple">
                            <div></div>
                        </div>
                    </div>
                </div>
                <div className="countdown-container">
                    <Circular
                      type="days"
                      size="200"
                      isOpen={this.props.store.isOpenAppShow}
                      date_end={this.props.store.currentEvent.date_end}
                      id={this.props.store.currentEvent._id} />
                    <Circular
                    isOpen={this.props.store.isOpenAppShow}
                        type="hours"
                        size="200"
                        date_end={this.props.store.currentEvent.date_end}
                        id={this.props.store.currentEvent._id}/>
                    <Circular
                        type="minutes"
                        size="200"
                        isOpen={this.props.store.isOpenAppShow}
                        date_end={this.props.store.currentEvent.date_end}
                        id={this.props.store.currentEvent._id} />
                    <Circular
                        type="seconds"
                        size="200"
                        isOpen={this.props.store.isOpenAppShow}
                        date_end={this.props.store.currentEvent.date_end}
                        id={this.props.store.currentEvent._id}/>
                    <Circular
                        type="milliseconds"
                        size="200"
                        isOpen={this.props.store.isOpenAppShow}
                        date_end={this.props.store.currentEvent.date_end}
                        id={this.props.store.currentEvent._id}/>
                </div>
                <IconButton
                  className="button-close"
                  onTouchTap={this.handleClose}><NavigationClose /></IconButton>
            </div>
          </Drawer>
        )
    }
}

export default connect(function(state) {
    return {
        store: {
            isOpenAppShow: state.isOpenAppShow,
            currentEvent: state.currentEvent,
        }
    }
})(AppShow);
