import React, {
  Component
} from 'react';
import Config from '../../config';
import { connect } from 'react-redux';
import { updateMessages } from '../../redux/action';
import TextField from 'material-ui/TextField';
import ListItem from 'material-ui/List/ListItem';
import Avatar from 'material-ui/Avatar';
import { sendMessage } from '../../blocks/api/message.api';

class ChatAction extends Component {
  constructor(props){
    super(props);
    this.state = {
        message: '',
        inputLength: 0,
        isActiveSendButton: false
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleMessageSend = this.handleMessageSend.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    this.setState({
        message: value,
        inputLength: value.length
    });
  }
  handleMessageSend() {
    if (!this.state.message || this.state.message.length > 200) return;

    // Call to send message api
    const params = {
      userId: this.props.userId,
      eventId: this.props.eventId,
      message: this.state.message
    }

    // Create object message
    let message = {
      content: params.message,
      user: {
        id: this.props.userId,
        avatar: this.props.avatar,
        name: this.props.name,
        isAdmin: this.props.isAdmin
      },
      time: Date.now()
    }

    // Call API
    sendMessage(params).then((res) => {
    });

    // Send object message to socket
    this.props.store.socket.emit('c send message', message);

    // Update state in store: currentEvent
    var { dispatch } = this.props;
    dispatch(updateMessages(message));

    // Reset input field value after send message
    this.setState({ message: '' });
  }
  handleKeyDown(e) {
    if(e.keyCode === 13){
      this.handleMessageSend();
    }
  }
  render() {
    // Active icon-send-message when user is typing
    let activeIcon = 'active';
    if (!this.state.message || this.state.message.length > 200) {
      activeIcon = 'deactive';
    }
    // Warning if message length > 200
    let mes;
    if (this.state.message.length >= 200) {
      mes = 'warning';
    }

    return(
      <div className="ChatAction">
        <ListItem className="ListItem"
          disabled={true}
          leftAvatar={
            <Avatar
              src={Config.BASE_URL_SERVER_AVATAR + this.props.avatar + '.png'}
              size={32}
            />}
        >
          <TextField
            floatingLabelText = {this.props.name}
            floatingLabelFixed = {true}
            className = "TextField"
            hintText = "Say something..."
            type = "text"
            maxLength="200"
            value = {this.state.message}
            onChange = {this.handleInputChange}
            onKeyDown = {this.handleKeyDown}
          />
        </ListItem>
        <i className={"material-icons IconSendMessage IconSendMessage-" + activeIcon} onClick={this.handleMessageSend}>send</i>
        <span className="ActionPlus">
          <span className={mes}>{this.state.message.length}</span>
          /200
        </span>
      </div>
    )
  }
}

export default connect((state) => {
  return {
    store: {
      socket: state.socket
    }
  }
})(ChatAction);
