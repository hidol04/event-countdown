import ReactDOM from 'react-dom';
import React, {
  Component
} from 'react';
import { ListItem } from 'material-ui/List';
import Config from '../../config';
import Avatar from 'material-ui/Avatar';

class ChatBoard extends Component {
  componentDidMount() {
    const node = ReactDOM.findDOMNode(this.messagesEnd);
    node.scrollIntoView({ behavior: "smooth" });
  }
  componentDidUpdate() {
    const node = ReactDOM.findDOMNode(this.messagesEnd);
    node.scrollIntoView({ behavior: "smooth" });
  }
  handleBrokenImage(e) {
    e.target.src = Config.BASE_URL_SERVER_AVATAR + "default.png";
    return true;
  }
  render() {
    let data = [];
    let { messages } = this.props;
    let total = messages.length;
    let limit = (total <= 80) ? total : 80;
    let i = total - limit;
    let isOwner;
    for(i; i < total; i++ ) {
      isOwner = (this.props.ownerId === messages[i].user.id) ? true : false;
      data.push(
        <ListItem
          key={i}
          disabled={true}
          className="ListItem"
          leftAvatar={
            <Avatar
              size={32}
              src={Config.BASE_URL_SERVER_AVATAR + messages[i].user.avatar + '.png'}
              onError={this.handleBrokenImage}
            />
          }
          secondaryText={
            <p className="Item">
              <span className={"name isOwner-" + isOwner}>{messages[i].user.name}</span>
              <span className="content">{messages[i].content}</span>
            </p>
          }
          secondaryTextLines={2}
        />
      )
    }

    return (
      <div className="ChatBoard">
          {data}
        <div style={{ float:"left", clear: "both" }}
           ref={(el) => { this.messagesEnd = el; }}>
        </div>
      </div>
    )
  }
}

export default ChatBoard;
