import React, {
    Component
} from 'react';
import { Redirect, Link } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import Authen from '../../blocks/api/authen.api';
import CircularLoader from '../Loader/circular.component';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            name: '',
            mail: '',
            password: '',
            redirectToReferrer: false,
            message: '',
            isShowProgress: false
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    handleKeyUp(e) {
      if(e.keyCode === 13){
        this.handleSubmit();
      }
    }
    handleSubmit(e) {
        // Check fields null
        if(this.state.name === '' || this.state.mail === '' || this.state.password === '') {
          this.setState({message: "* Username, mail, password are required"});
          return;
        }

        this.setState({ isShowProgress: true });

        let params = {
            username: this.state.username,
            name: this.state.name,
            mail: this.state.mail,
            password: this.state.password,
        }

        Authen.register(params).then((res) => {
            if (!res.body.success) {
              // Get fields error
              let { fields } = res.body.error;
              this.setState({
                message: "* Sign up failed.",
                username_error: fields.username,
                name_error: fields.name,
                mail_error: fields.mail,
                password_error: fields.password,
                isShowProgress: false
              });
            } else {
                // Signup success
                // Set state
                this.setState({
                    message: '',
                    redirectToReferrer: true,
                });
            }
        });

    }
    render() {
        // Redirect
        if(this.state.redirectToReferrer) {
            return (
                <Redirect to="/login"/>
            )
        }
        return (
          <MuiThemeProvider>
            <div className="authen">
              <Paper className="Paper" zDepth={1} rounded={false} transitionEnabled={false}>
                <h3>Sign Up</h3>
                <Divider />
                <form onSubmit={this.handleSubmit}>
                  <TextField
                    value={this.state.username}
                    floatingLabelText="Username"
                    name = "username"
                    errorText={this.state.name_error}
                    onChange={this.handleInputChange}
                    onKeyUp = {this.handleKeyUp}
                  /><br />
                  <TextField
                    value={this.state.name}
                    floatingLabelText="Name"
                    name = "name"
                    errorText={this.state.name_error}
                    onChange={this.handleInputChange}
                    onKeyUp = {this.handleKeyUp}
                  /><br />
                  <TextField
                    value={this.state.mail}
                    floatingLabelText="Mail"
                    name = "mail"
                    errorText={this.state.mail_error}
                    onChange={this.handleInputChange}
                    onKeyUp = {this.handleKeyUp}
                  /><br />
                  <TextField
                    value={this.state.password}
                    floatingLabelText="Password"
                    name = "password"
                    type="password"
                    errorText={this.state.password_error}
                    onChange={this.handleInputChange}
                    onKeyUp = {this.handleKeyUp}
                  /><br />
                  <span className="failed-message">{this.state.message}</span><br />
                  <RaisedButton
                    className="register-button"
                    label="Register"
                    style={{marginTop: '10px'}}
                    onTouchTap={this.handleSubmit}
                    primary={true}/>
                  <CircularLoader isShow={this.state.isShowProgress} />
                </form>
                <Divider />
                <span className="register-suggest">* Have an account? <Link to={`/login`}>Log In.</Link></span>
              </Paper>
            </div>
          </MuiThemeProvider>
        )
    }
}

export default Register;
