import React, {
    Component
} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Authen from '../../blocks/api/authen.api';
import { changeUser } from '../../redux/action';
import { getToken, setToken, setUserId } from '../../blocks/session/session.js';
import CircularLoader from '../Loader/circular.component';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            redirectToReferrer: false,
            message: '',
            isShowProgress: false,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleInputChange(e) {
      const target = e.target;
      const value = target.value;
      const name = target.name;

      this.setState({
          [name]: value
      });
    }
    handleKeyUp(e) {
      if(e.keyCode === 13){
        this.handleSubmit();
      }
    }
    handleSubmit() {
      // Check fields null
      if(this.state.username === '' || this.state.password === '') {
        this.setState({message: "* Username or password is required"});
        return;
      }

      this.setState({ isShowProgress: true });

      let params = {
          username: this.state.username,
          password: this.state.password
      }

      Authen.login(params).then((res) => {
          this.setState({ isShowProgress: false });

          if (!res.body.success) {
              //Authen error
              this.setState({
                message: "* Username or password is incorrect",
                isShowProgress: false,
              });
          } else {
              // Authen success
              // Save data in localStorage
              setToken(res.body.success.token);
              setUserId(res.body.success.data._id);
              // Update state in store: user
              var { dispatch } = this.props;
              dispatch(changeUser(res.body.success.data));
              // Set state
              this.setState({
                  message: '',
                  redirectToReferrer: true,
              });
          }
      });

    }
    render() {
        // Redirect if login success or token exists
        if(this.state.redirectToReferrer || getToken()) {
            return (
                <Redirect to="/" />
            )
        }
        return (
          <MuiThemeProvider>
            <div className="authen">
              <Paper className="Paper" zDepth={1} rounded={false} transitionEnabled={false}>
                <h3>Log In</h3>
                <Divider />
                <form className="login-form">
                  <TextField
                    value={this.state.username}
                    floatingLabelText="Username"
                    name = "username"
                    onChange={this.handleInputChange}
                    onKeyUp = {this.handleKeyUp}
                  /><br />
                  <TextField
                    value={this.state.password}
                    floatingLabelText="Password"
                    name = "password"
                    type="password"
                    onChange={this.handleInputChange}
                    onKeyUp = {this.handleKeyUp}
                  /><br />
                  <span className="failed-message">{this.state.message}</span><br />
                  <RaisedButton
                    className="login-button"
                    label="Login"
                    style={{marginTop: '10px'}}
                    onTouchTap={this.handleSubmit}
                    primary={true}/>
                  <CircularLoader isShow={this.state.isShowProgress} />
                </form>
                <Divider />
                <span className="register-suggest">* Don’t have an account? <Link to={`/register`}>Create an EC account.</Link></span>
              </Paper>
            </div>
          </MuiThemeProvider>
        )
    }
}

export default connect()(Login);
