import React, {
    Component
} from 'react';
import LinearProgress from 'material-ui/LinearProgress';

class LinearIndeterminate extends Component {
  render() {
    if(!this.props.isShow) return null;
    
    return (
      <LinearProgress mode="indeterminate" />
    );
  }
}

export default LinearIndeterminate;
