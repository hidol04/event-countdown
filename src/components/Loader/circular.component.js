import React, {
    Component
} from 'react';
import CircularProgress from 'material-ui/CircularProgress';

class CircularLoader extends Component {
  render() {
    if(!this.props.isShow) return null;
    
    return (
      <div className="loader" >
        <CircularProgress size={25} thickness={2} />
      </div>
    );
  }
}

export default CircularLoader;
