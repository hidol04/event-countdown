import React, {
    Component
} from 'react';
import { connect } from 'react-redux';
import { toggleModalUpdateProfile, changeUser } from '../../redux/action';
import { updateUser } from '../../blocks/api/user.api';
import Config from '../../config';
import PREFIX_FILE_UPLOAD from '../../blocks/prefix-file-upload';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import CircularLoader from '../Loader/circular.component';

class UpdateProfileModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
          imagePreviewUrl: '',
          isShowProgress: false
        }

        this.handleClose = this.handleClose.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentWillReceiveProps(nextProps) {
      if(nextProps.store.user !== null)
        this.setState({
          id: nextProps.store.user._id,
          name: nextProps.store.user.name,
          mail: nextProps.store.user.mail,
          avatar: nextProps.store.user.avatar,
          isShowProgress: false
        });
    }
    handleClose() {
      var { dispatch } = this.props;
      dispatch(toggleModalUpdateProfile(false));
    }
    handleInputChange(e) {
      const target = e.target;
      const name = target.name;
      const value = target.value;

      this.setState({
        [name]: value
      });
    }
    handleFileChange(e) {
      var self = this;
      var reader = new FileReader();
      var file = e.target.files[0];

      reader.onload = function(upload) {
        self.setState({
          file: file,
          imagePreviewUrl: reader.result
        });
      }

      reader.readAsDataURL(file);
    }
    handleSubmit(e) {
      e.preventDefault();

      this.setState({ isShowProgress: true });

      let params = new FormData();
      params.append("id", this.state.id);
      // Add field to params if it had changed
      if(this.state.name !== this.props.store.user.name) params.append("name", this.state.name);
      if(this.state.mail !== this.props.store.user.mail) params.append("mail", this.state.mail);
      if(this.state.password) params.append("password", this.state.password);
      if(this.state.file) params.append(PREFIX_FILE_UPLOAD.avatar, this.state.file);

      // Call to update event api
      updateUser(params).then((res) => {
        var { dispatch } = this.props;
        dispatch(changeUser(res.body.success.data));

        // Close modal
        this.handleClose();
      })
    }
    handleUploadFile() {
      document.getElementById("input-file-avatar").click();
    }
    render() {
      if(this.props.store.user === null || !this.state.avatar) return null;

      // Get image preview
      let { imagePreviewUrl } = this.state;
      let imagePreview = null;
      if (imagePreviewUrl) {
        imagePreview = (<img width="200" height="200" src={imagePreviewUrl} alt=""/>);
      } else {
        imagePreview = (<img width="200" height="200" src={Config.BASE_URL_SERVER_AVATAR + this.state.avatar + '.png'} alt=""/>);
      }

      return (
        <Drawer
          className="UpdateProfileModal"
          openSecondary={false} width={'100%'}
          animated={false}
          iconStyleRight={{'margin-top': 0}}
          open={this.props.store.isOpenModalUpdateProfile}>
          <AppBar
            title="Edit your profile"
            showMenuIconButton={false}
            iconElementRight={<IconButton><NavigationClose /></IconButton>}
            onRightIconButtonTouchTap={this.handleClose}
          />
          <div className="form-container">
            <div className="form-container-left">
              <TextField
                value={this.state.name}
                floatingLabelText="Name"
                name = "name"
                onChange={this.handleInputChange}
              /><br />
              <TextField
                value={this.state.mail}
                floatingLabelText="Email"
                name = "mail"
                onChange={this.handleInputChange}
              /><br />
              <TextField
                floatingLabelText="Password"
                name = "password"
                onChange={this.handleInputChange}
              /><br />
            </div>
            <div className="form-container-right">
              <div className="avatar" onClick={this.handleUploadFile}>
                <input id="input-file-avatar" type="file" onChange={this.handleFileChange} style={{display: 'none'}}/>
                { imagePreview }
                <div className="overlay">
                  <div className="text">
                    <i className="material-icons">add_a_photo</i><br />
                    Change your profile
                  </div>
                </div>
              </div>
            </div>
            <div className="form-container-bottom">
              <CircularLoader isShow={this.state.isShowProgress} />
              <RaisedButton
                className="cancel"
                label="Cancel"
                disableTouchRipple={true}
                onTouchTap={this.handleClose}
              />
              <RaisedButton
                className="save"
                label="Save changes"
                disableTouchRipple={true}
                onTouchTap={this.handleSubmit}
              />
            </div>
          </div>
        </Drawer>
      )
    }
}

export default connect((state) => {
  return {
    store: {
      user: state.user,
      isOpenModalUpdateProfile: state.isOpenModalUpdateProfile
    }
  }
})(UpdateProfileModal);
