import React, {
    Component
} from 'react';
// import TextField from 'material-ui/TextField';
import { connect } from 'react-redux';
import { updateUser } from '../../blocks/api/user.api';

class TestUploadFile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            file: ''
        };
        this.closeModal = this.closeModal.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    closeModal() {
    }
    handleInputChange(e) {
      const target = e.target;
      const value = target.value;
      const name = target.name;

      this.setState({ id: value });
    }
    handleFileChange(e) {
      var self = this;
      var reader = new FileReader();
      var file = e.target.files[0];

      reader.onload = function(upload) {
        self.setState({
          file: file,
        });
      }

      reader.readAsDataURL(file);
    }
    handleSubmit(e) {
      e.preventDefault();
      console.log(this.state.file);
      let fictitiousFile = {
        File: {
          name: 'hello',
          type: "image/png",
        }
      }

      let params = new FormData();
      params.append("avatar", this.state.file);
      params.append("id", this.state.id);
      params.append("currentUserName", "ThisIsCurrentName");

      // Call to update event api
      updateUser(params).then((res) => {
        // console.log(params);
        console.log(res);
      })

    }
    render() {
        return (
            <div>
            <form onSubmit={this.handleSubmit} encType="multipart/form-data" method="post">
              <input
                type="text"
                name = "id"
                value = {this.state.id}
                onChange = {this.handleInputChange} />
              <input type="file" name="avatar" onChange={this.handleFileChange} />
              <button>Submit</button>
            </form>
            </div>
        )
    }
}

export default connect((state) => {
  return {
    state: {
      user: state.user
    }
  }
})(TestUploadFile);
