import React, { Component } from 'react';
import { connect} from 'react-redux';
import { toggleModalMyEvents } from '../../redux/action';
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import Divider from 'material-ui/Divider';
import Circular from '../App/circular.component';

class ShowMyEvents extends Component {
    constructor(props) {
        super(props);

        this.handleClose = this.handleClose.bind(this);
    }
    handleClose() {
      var { dispatch } = this.props;
      dispatch(toggleModalMyEvents(false));
    }
    render() {
      if (this.props.store.myEvents === null ||
        !this.props.store.isOpenModalMyEvents) return null;

      var self = this;
      return (
        <Drawer
          className="AppShow ShowMyEvents"
          openSecondary={false} width={'100%'}
          animated={false}
          iconStyleRight={{'margin-top': 0}}
          open={this.props.store.isOpenModalMyEvents}
        >
          <div className="AppShow-Content">
            {this.props.store.myEvents.map(function(event, i) {
            return (
              <div key={i}>
                <div className="countdown-container">
                  <div className="Info">
                    <span>{event.name}</span><br />
                    {(new Date(event.date_end)).getFullYear() + " / " +
                      ((new Date(event.date_end)).getMonth() + 1) + " / " +
                      (new Date(event.date_end)).getDate()}
                  </div>
                  <Circular
                    size="150"
                    isOpen={self.props.store.isOpenModalMyEvents}
                    type="days"
                    id={event._id}
                    date_end={event.date_end} />
                  <Circular
                    size="150"
                    isOpen={self.props.store.isOpenModalMyEvents}
                    type="hours"
                    id={event._id}
                    date_end={event.date_end} />
                  <Circular
                    size="150"
                    isOpen={self.props.store.isOpenModalMyEvents}
                    type="minutes"
                    id={event._id}
                    date_end={event.date_end} />
                  <Circular
                    size="150"
                    isOpen={self.props.store.isOpenModalMyEvents}
                    type="seconds"
                    id={event._id}
                    date_end={event.date_end} />
                </div>
                <Divider />
              </div>
              )
          })}
          </div>
          <IconButton
            className="button-close"
            onTouchTap={this.handleClose}><NavigationClose />
          </IconButton>
        </Drawer>
      )
    }
}

export default connect(function(state) {
    return {
        store: {
            isOpenModalMyEvents: state.isOpenModalMyEvents,
            myEvents: state.myEvents
        }
    }
})(ShowMyEvents);
