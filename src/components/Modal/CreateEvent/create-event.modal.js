import React, {
    Component
} from 'react';
import { Redirect } from 'react-router-dom';
import Config from '../../../config';
import Drawer from 'material-ui/Drawer';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import RaisedButton from 'material-ui/RaisedButton';
import PREFIX_FILE_UPLOAD from '../../../blocks/prefix-file-upload';
import { connect } from 'react-redux';
import { toggleModalCreateEvent, addMyEvents } from '../../../redux/action';
import { createEvent } from '../../../blocks/api/event.api';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import Subheader from 'material-ui/Subheader';
import { getUserId } from '../../../blocks/session/session';
import CircularLoader from '../../Loader/circular.component';
import HashTagsForm from './hashtags.form';

class CreateEventModal extends Component {
  constructor(props) {
      super(props);

      // Set Date & Time default
      const minDate = new Date();
      const maxDate = new Date();
      minDate.setFullYear(minDate.getFullYear());
      minDate.setHours(0, 0, 0, 0);
      maxDate.setHours(0, 0, 0, 0);

      this.state = {
        imagePreviewUrl: '',
        minDate: minDate,
        maxDate: maxDate,
        startTime: null,
        endTime: null,
        redirectToReferrer: false,
        eventId: '',
        isShowProgress: false,
      };
      this.handleRequestClose = this.handleRequestClose.bind(this);
      this.handleInputChange = this.handleInputChange.bind(this);
      this.handleFileChange = this.handleFileChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChangeMinDate = this.handleChangeMinDate.bind(this);
      this.handleChangeMaxDate = this.handleChangeMaxDate.bind(this);
      this.handleChangeTimeStartPicker = this.handleChangeTimeStartPicker.bind(this);
      this.handleChangeTimeEndPicker = this.handleChangeTimeEndPicker.bind(this);
      this.getHashTags = this.getHashTags.bind(this);
  }
  handleChangeMinDate = (event, date) => {
    this.setState({
      minDate: date,
    });
  };
  componentWillReceiveProps(nextProps) {
    this.setState({
      redirectToReferrer: false,
      isShowProgress: false
    });
  }
  handleChangeMaxDate = (event, date) => {
    this.setState({
      maxDate: date,
    });
  };
  handleRequestClose() {
    var { dispatch } = this.props;
    dispatch(toggleModalCreateEvent(false));
  }
  handleInputChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      this.setState({
          [name]: value
      });
  }
  handleUploadFile() {
    document.getElementById("input-file-event").click();
  }
  handleFileChange(e) {
    var self = this;
    var reader = new FileReader();
    var file = e.target.files[0];

    reader.onload = function(upload) {
      self.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file);
  }
  handleChangeTimeStartPicker(e, time) {
    this.setState({startTime: time});
  }
  handleChangeTimeEndPicker(e, time) {
    this.setState({endTime: time});
  }
  getHashTags(hashtags) {
    this.setState({ hashtags: hashtags });
  }
  handleSubmit() {
    if(!this.state.name) {
      this.setState({ err_mess_name: "Field `name` is required" });
      return;
    }

    this.setState({ isShowProgress: true });

    // Check hours and minute
    if(this.state.startTime !== null) {
      let startHours = this.state.startTime.getHours();
      let startMinutes = this.state.startTime.getMinutes();
      this.state.minDate.setHours(startHours, startMinutes, 0 , 0);
    }
    if(this.state.endTime !== null) {
      let endHours = this.state.endTime.getHours();
      let endMinutes = this.state.endTime.getMinutes();
      this.state.maxDate.setHours(endHours, endMinutes, 0 , 0);
    }

    // Params to request
    let params = new FormData();
    params.append("id", getUserId());
    if(this.state.name) params.append("name", this.state.name);
    if(this.state.location) params.append("location", this.state.location);
    params.append("date_start", this.state.minDate);
    params.append("date_end", this.state.maxDate);
    if(this.state.description) params.append("description", this.state.description);
    if(this.state.file) params.append(PREFIX_FILE_UPLOAD.event, this.state.file);
    if(this.state.hashtags) {
      for (var i = 0; i < this.state.hashtags.length; i++) {
        params.append('hashtags', this.state.hashtags[i]);
      }
    }


    // Call to create event api
    createEvent(params).then((res) => {
      if(res.body.success) {
        this.handleRequestClose();
        // Redirect to event detail
        this.setState({
          redirectToReferrer: true,
          eventId: res.body.success.data._id
        });

        // Update store: myEvents && isOpenModalCreateEvent
        var { dispatch } = this.props;
        dispatch(addMyEvents(res.body.success.data));
        dispatch(toggleModalCreateEvent(false));
      }
    });
  }
  render() {
    // Redirect if create event successed
    if(this.state.redirectToReferrer && this.state.eventId !== null) {
        return (
            <Redirect to={`/countdown/launch?eventId=` + this.state.eventId} />
        )
    }

    // Get image preview
    let { imagePreviewUrl } = this.state;
    let imagePreview = null;
    if (imagePreviewUrl) {
      imagePreview = (<img width="500" height="200" src={imagePreviewUrl} alt=""/>);
    } else {
      imagePreview = (<img width="500" height="200" src={Config.BASE_URL_SERVER_EVENT + 'default.png'} alt=""/>);
    }

    return (
      <Drawer
        className="CreateEventModal"
        openSecondary={false} width={'100%'}
        animated={false}
        iconStyleRight={{'margin-top': 0}}
        open={this.props.store.isOpenModalCreateEvent}>
        <AppBar
          title="Create Event"
          className="AppBar"
          showMenuIconButton={false}
          iconElementRight={<IconButton><NavigationClose /></IconButton>}
          onRightIconButtonTouchTap={this.handleRequestClose}
        />
        <div className="CreateEventModal-Content">
          <div className="step1">
            <div className="row">
              <TextField
                className="name-field"
                floatingLabelText="Event name"
                name = "name"
                hintText="Add a short, clear name"
                errorText={(this.state.err_mess_name) ? this.state.err_mess_name : ""}
                onChange={this.handleInputChange}
              />
              <TextField
                className="location-field"
                floatingLabelText="Location"
                name = "location"
                hintText="Include a place or address"
                onChange={this.handleInputChange}
              /><br />
            </div>
            <div className="row">
              <DatePicker
                className="date_start"
                onChange={this.handleChangeMinDate}
                floatingLabelText="Date Start"
                defaultDate={this.state.minDate}
              />
              <TimePicker
                name="time_start"
                className="time_start"
                hintText="Time Start"
                value={this.state.startTime}
                onChange={this.handleChangeTimeStartPicker}
              />
            </div>
            <div className="row">
              <DatePicker
                className="date_end"
                onChange={this.handleChangeMaxDate}
                floatingLabelText="Date End"
                defaultDate={this.state.maxDate}
              />
              <TimePicker
                name="time_end"
                className="time_end"
                hintText="Time End"
                value={this.state.endTime}
                onChange={this.handleChangeTimeEndPicker}
              />
            </div>
            <div className="row">
              <HashTagsForm
                className="hashtags"
                getHashTags={this.getHashTags}
                isOpen={this.props.store.isOpenModalCreateEvent} />
              <TextField
                className="description"
                floatingLabelText="Description"
                hintText="Hint Text"
                name="description"
                multiLine={true}
                rowsMax={10}
                onChange={this.handleInputChange}
              />
            </div>
          </div>
          <div className="step2">
            <div className="Event-Cover">
              <Subheader>Event photo</Subheader>
              <div className="Event-Cover" onClick={this.handleUploadFile}>
                <input id="input-file-event" type="file" onChange={this.handleFileChange} style={{display: 'none'}}/>
                { imagePreview }
                <div className="overlay">
                  <div className="text">
                    <i className="material-icons">add_a_photo</i><br />
                    Upload event cover
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="CreateEventModal-Footer">
          <CircularLoader isShow={this.state.isShowProgress} />
          <RaisedButton
            className="cancel"
            label="Cancel"
            disableTouchRipple={true}
            onTouchTap={this.handleRequestClose}
          />
          <RaisedButton
            className="save"
            label="Save changes"
            disableTouchRipple={true}
            onTouchTap={this.handleSubmit}
          />
        </div>
      </Drawer>
    )
  }
}

export default connect((state) => {
  return {
    store: {
      isOpenModalCreateEvent: state.isOpenModalCreateEvent,
    }
  }
})(CreateEventModal);
