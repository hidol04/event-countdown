import React, {
    Component
} from 'react';
import AutoComplete from 'material-ui/AutoComplete';
import Chip from 'material-ui/Chip';
import { getHashtags } from '../../../blocks/api/hashtag.api';

class HashTagsForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      chosenHashtags: [],
      hashtags: [],
    }

    this.handleChoose = this.handleChoose.bind(this);
    this.handleUpdateInput = this.handleUpdateInput.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if(this.props.isOpen !== nextProps.isOpen && !nextProps.isOpen) {
      // Reset
      this.setState({
        chosenHashtags: [],
      })

      // Reset input value
      let input = document.getElementsByClassName('hashtag-field')[0].getElementsByTagName('input')[0];
      input.value = '';
      input.setAttribute('value', '');
      console.log(input);
    }
  }
  componentWillMount() {
    getHashtags().then((res) => {
      let i = 0;
      let total = res.body.success.data.length;
      let data = [];
      for(i; i < total; i++) {
        data.push(res.body.success.data[i].name);
      };
      this.setState({ hashtags: data });
    });
  }
  handleChoose(hashtag, i) {
    this.state.chosenHashtags.push(hashtag);
    this.setState({ chosenHashtags: this.state.chosenHashtags }, () => {
      this.props.getHashTags(this.state.chosenHashtags);
    });

    let selector = document.getElementsByClassName('hashtag-field')[0];
    let input = selector.getElementsByTagName('input')[0];
    input.focus();
    input.value = '';

  }
  handleRequestDelete(hashtag) {
    const chipToDelete = this.state.chosenHashtags.map((chip) => chip).indexOf(hashtag);
    this.state.chosenHashtags.splice(chipToDelete, 1);
    this.setState({ chosenHashtags: this.state.chosenHashtags }, () => {
      this.props.getHashTags(this.state.chosenHashtags);
    });
  }
  handleUpdateInput(searchText, dataSource, params) {
    if(dataSource[0] === "") {
      dataSource.splice(0, 1);
      this.setState({ hashtags: dataSource });
    } else {
      let arr = [];
      arr[0] = searchText;
      arr = Array.from(dataSource);
      this.setState({ hashtags: arr });
    }
  }
  handleTouchTap() {

  }
  render() {
    // List suggest hashtags
    if(this.state.hashtags === null) return null;

    // Data Source
    let data = this.state.hashtags;

    return (
      <div className="hashtags">
        <AutoComplete
          className="hashtag-field"
          floatingLabelText="Type 'hashtag'"
          filter={AutoComplete.fuzzyFilter}
          dataSource={data}
          onUpdateInput={this.handleUpdateInput}
          maxSearchResults={5}
          onNewRequest={this.handleChoose}
          animated={false}
        />

        <div>
          {this.state.chosenHashtags.map((hashtag, i) => {
            return(
              <Chip
                key={i}
                onRequestDelete={() => this.handleRequestDelete(hashtag)}
                onTouchTap={this.handleTouchTap}
              >
                {hashtag}
              </Chip>
            )
          })}
        </div>
      </div>
    )
  }
}

export default HashTagsForm;
