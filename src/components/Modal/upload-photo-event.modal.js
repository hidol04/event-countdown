import React, {
    Component
} from 'react';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import CircularLoader from '../Loader/circular.component';
import { uploadPhotosApi } from '../../blocks/api/event.api';
import { uploadPhotos } from '../../redux/action';

class UploadPhotosEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      files: [],
      imagesPreviewUrl: [],
      message_error: '',
      isUploading: false
    }

    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleClose() {
    this.props.toggle();
  };
  handleUploadFile() {
    document.getElementById("input-photos-event").click();
  }
  setupReader(file) {
    var self = this;
    var reader = new FileReader();
    reader.onload = function(e) {
        // var text = e.target.result;
        var files_temp = self.state.files.concat(file);
        var imagesPreviewUrl_temp = self.state.imagesPreviewUrl.concat(reader.result);
        self.setState({
          files: files_temp,
          imagesPreviewUrl: imagesPreviewUrl_temp
        });
    }
    reader.readAsDataURL(file);
  }

  handleFileChange(e) {
    let files = e.target.files;
    let i = 0;
    for (i; i < files.length; i++) {
      this.setupReader(files[i]);
    }
  }
  handleSubmit() {
    if(this.state.files.length === 0) return;

    // Set isUploading state
    this.setState({ isUploading: true });

    // Insert data into formdata
    let params = new FormData();
    params.append("eventId", this.props.data._id);
    params.append("userId", this.props.user._id);
    let i = 0;
    for(i; i < this.state.files.length; i++) {
      params.append("photos", this.state.files[i]);
    }

    // Call API
    uploadPhotosApi(params).then((res) => {
      if(!res.body.success) {
        this.setState({ message_error: "Failed to upload photos."});
        return;
      }

      // Reset state imagesPreviewUrl
      this.setState({
        imagesPreviewUrl: [],
        isUploading: false
      });

      // Update store: currentEventReducer
      let i = 0;
      let photosRes = res.body.success.data;
      let photosCount = photosRes.length;
      var { dispatch } = this.props;
      for(i; i < photosCount; i++) {
        dispatch(uploadPhotos(photosRes[i]));
      }

      // Close modal
      this.handleClose();
    });
  }
  render() {
    const actions = [
      <CircularLoader isShow={this.state.isUploading}/>,
      <FlatButton
        className="Button-Cancel"
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        className="Button-Submit"
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleSubmit}
      />,
    ];

    // Get photos preview
    let { imagesPreviewUrl } = this.state;
    let imagesPreview = [];
    if (imagesPreviewUrl) {
      let i = 0;
      for(i; i < imagesPreviewUrl.length; i++) {
        imagesPreview.push(<img width="150" height="150" src={imagesPreviewUrl[i]} alt=""/>);
      }
    }

    return (
      <Dialog
        className="UploadPhotosEvent"
        title="Upload photos"
        modal={false}
        open={this.props.isOpen}
        onRequestClose={this.handleClose}
        actions={actions}
        bodyStyle={{paddingBottom: '10px'}}
        actionsContainerStyle={{position: 'relative', paddingTop: '0'}}
      >
        <div className="row">
          <div className="photo_add">
            <div className="overlay" onClick={this.handleUploadFile}>
              <div className="text">
                <i className="material-icons">add_a_photo</i><br /><br />
                Add photos
              </div>
            </div>
          </div>
          {imagesPreview.map((photo, i) => {
            return (
              <div key={i} className="preview">{photo}</div>
            )
          })}
          <div className="photos_preview">
          </div>
        </div>
        <input id="input-photos-event" type="file" multiple onChange={this.handleFileChange} style={{display: 'none'}}/>
      </Dialog>
    )
  }
}

export default connect()(UploadPhotosEvent);
