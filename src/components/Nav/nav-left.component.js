import React, {
    Component
} from 'react';
import NavListEvents from './nav-list-events.component';
import NavUser from './nav-user.component';
import { List } from 'material-ui/List';
import Drawer from 'material-ui/Drawer';

class NavLeft extends Component {
  render() {
    const style = {
      padding: 0
    }
    return (
        <Drawer
          className="NavLeft"
          disableSwipeToOpen={true}
          open={true}
          zDepth={1}>
          <List style={style}>
            <NavUser />
            <NavListEvents />
          </List>
        </Drawer>
    )
  }
}

export default NavLeft;
