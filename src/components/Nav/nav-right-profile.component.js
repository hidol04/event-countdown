import React, {
    Component
} from 'react';
import { connect } from 'react-redux';
import { toggleNavRightProfile, toggleModalUpdateProfile } from '../../redux/action';
import RaisedButton from 'material-ui/RaisedButton';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import Config from '../../config';

class NavRightProfile extends Component {
    constructor(props) {
      super(props);

      this.handleOpenModalUpdateProfile = this.handleOpenModalUpdateProfile.bind(this);
      this.handleCloseNavRightProfile = this.handleCloseNavRightProfile.bind(this);
    }
    handleOpenModalUpdateProfile() {
      var { dispatch } = this.props;
      dispatch(toggleModalUpdateProfile(true));
    }
    handleCloseNavRightProfile() {
      var { dispatch } = this.props;
      dispatch(toggleNavRightProfile(false));
    }
    handleBrokenImage(e) {
      e.target.src = Config.BASE_URL_SERVER_AVATAR + "default.png";
      return true;
    }
    render() {
      if(this.props.store.user === null) return null;

      return (
        <Drawer className="NavRightProfile" width={400} openSecondary={true} open={this.props.store.isOpenNavRightProfile}>
          <AppBar
            title="Profile"
            className="AppBar"
            showMenuIconButton={false}
            iconElementRight={<IconButton><NavigationClose /></IconButton>}
            iconStyleRight={{'marginTop': 0}}
            onRightIconButtonTouchTap={this.handleCloseNavRightProfile}
          />
          <div className="Main">
            <div className="Header">
              <div className="Header-Avatar">
                <img
                  width="100%"
                  src={Config.BASE_URL_SERVER_AVATAR + this.props.store.user.avatar + '.png'}
                  alt=""
                  onError={this.handleBrokenImage}
                />
              </div>
              <div className="Header-Info">
                <span className="name">{this.props.store.user.name}<i className="material-icons">fiber_manual_record</i></span><br /><br />
                <RaisedButton
                  className="Header-Info-Button"
                  label="Edit Profile"
                  labelPosition="before"
                  disableTouchRipple={true}
                  onTouchTap={this.handleOpenModalUpdateProfile}
                >
                </RaisedButton>
                <RaisedButton
                  className="Header-Info-Button action"
                  disableTouchRipple={true}
                  icon={<i className="material-icons">keyboard_arrow_down</i>}
                >
                </RaisedButton>
              </div>
            </div>
            <Divider className="Divider"/>
            <div className="Info">
              <table>
                <tbody>
                  <tr>
                    <td className="field">Username</td>
                    <td>admin</td>
                  </tr>
                  <tr>
                    <td className="field">Timezone</td>
                    <td>GTM + 7</td>
                  </tr>
                  <tr>
                    <td className="field">Going</td>
                    <td>4</td>
                  </tr>
                  <tr>
                    <td className="field">Interested</td>
                    <td>4</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </Drawer>
      )
    }
}

export default connect((state) => {
  return {
    store: {
      user: state.user,
      isOpenNavRightProfile: state.isOpenNavRightProfile
    }
  }
})(NavRightProfile);
