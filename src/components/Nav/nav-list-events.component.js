import React, {
    Component
} from 'react';
import ListEventsNav from '../Event/list-events-nav.component';
import ListEventsFollowNav from '../Event/list-events-follow-nav.component';
import Subheader from 'material-ui/Subheader';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Paper from 'material-ui/Paper';

class NavListEvents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: 'All'
    }
    this.handleFilterFollowEvents = this.handleFilterFollowEvents.bind(this);
  }
  handleFilterFollowEvents(e) {
    this.setState({
      filter: e.target.innerHTML
    });
  }
  render() {
    return (
      <div className="NavListEvents">
        <Paper zDepth={1} rounded={false} transitionEnabled={false}>
          <Subheader className="NavListEvents-Subheader">
            <div className="NavListEvents-Subheader-Icon"><i className="material-icons">star</i></div>
            My Events
            <IconMenu
              className="IconMenu"
              animated={false}
              touchTapCloseDelay={1}
              iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
              anchorOrigin={{horizontal: 'left', vertical: 'top'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}>
                  <MenuItem disableTouchRipple={true} className="NavListEvents-IconMenu-MenuItem" primaryText="Refresh" />
                  <MenuItem disableTouchRipple={true} className="NavListEvents-IconMenu-MenuItem" primaryText="Sort" />
            </IconMenu>
          </Subheader>
          <ListEventsNav />
        </Paper>
        <Paper zDepth={1} rounded={false} transitionEnabled={false}>
          <Subheader className="NavListEvents-Subheader">
            <div className="NavListEvents-Subheader-Icon"><i className="material-icons">star</i></div>
            {'Subscription (' + this.state.filter + ')' }
            <IconMenu
              className="IconMenu"
              animated={false}
              iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
              anchorOrigin={{horizontal: 'left', vertical: 'top'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}>
                <MenuItem
                  onTouchTap={this.handleFilterFollowEvents}
                  className="NavListEvents-IconMenu-MenuItem"
                  value="all"
                  primaryText="All" />
                <MenuItem
                  onTouchTap={this.handleFilterFollowEvents}
                  className="NavListEvents-IconMenu-MenuItem"
                  value="going"
                  primaryText="Going" />
                <MenuItem
                  onTouchTap={this.handleFilterFollowEvents}
                  className="NavListEvents-IconMenu-MenuItem"
                  value="interested"
                  primaryText="Interested" />
            </IconMenu>
          </Subheader>
          <ListEventsFollowNav filter={this.state.filter} />
        </Paper>
      </div>
    )
  }
}

export default NavListEvents;
