import React, {
    Component
} from 'react';
import Drawer from 'material-ui/Drawer';
import FontIcon from 'material-ui/FontIcon';
import ChatNav from './nav-chat.component';

class NavRight extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };
        this.toggleRightNav = this.toggleRightNav.bind(this);
    }
    toggleRightNav() {
      this.setState({ isOpen: !this.state.isOpen });
    }
    render() {
      const iconStyles = {
        marginTop: 15,
        marginLeft: 6,
        fontSize: 20
      };

      return (
        <div className="NavRight">
          <div id="toggle-right" className={"toggle toggle-" + this.state.isOpen} onClick={this.toggleRightNav}>
            <FontIcon className={"material-icons chat " + !this.state.isOpen} style={iconStyles} color="#E2E2E2">question_answer</FontIcon>
          </div>
          <Drawer
            width={394}
            zDepth={1}
            openSecondary={true}
            open={this.state.isOpen}
            disableSwipeToOpen={true}>
            <ChatNav toggleRightNav={this.toggleRightNav} />
          </Drawer>
        </div>
      )
    }
}

export default NavRight;
