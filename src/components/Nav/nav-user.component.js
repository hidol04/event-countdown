import React, {
    Component
} from 'react';
import { connect } from 'react-redux';
import { toggleNavRightProfile, toggleModalCreateEvent, clearUser, toggleModalMyEvents } from '../../redux/action';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Popover from 'material-ui/Popover';
import Paper from 'material-ui/Paper';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { clearToken, clearUserId } from '../../blocks/session/session.js';
import { Redirect } from 'react-router-dom';

class NavUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isOpenPopover: false,
          redirectToReferrer: false,
        };
        this.handleTouchTap = this.handleTouchTap.bind(this);
        this.handleRequestClose = this.handleRequestClose.bind(this);
        this.handleOpenNavRightProfile = this.handleOpenNavRightProfile.bind(this);
        this.handleOpenModalCreateEvent = this.handleOpenModalCreateEvent.bind(this);
        this.handleOpenModalMyEvents = this.handleOpenModalMyEvents.bind(this);
        this.handleSignOut = this.handleSignOut.bind(this);
    }
    handleTouchTap(e) {
      e.preventDefault();
      this.setState({
        isOpenPopover: true,
        anchorEl: e.currentTarget
      });
    }
    handleRequestClose(e) {
      this.setState({ isOpenPopover: false });
    }
    handleOpenNavRightProfile(e) {
      var { dispatch } = this.props;
      dispatch(toggleNavRightProfile(true));
    }
    handleOpenModalCreateEvent() {
      var { dispatch } = this.props;
      dispatch(toggleModalCreateEvent(true));
    }
    handleOpenModalMyEvents() {
      var { dispatch } = this.props;
      dispatch(toggleModalMyEvents(true));
    }
    handleSignOut() {
      // Clear session
      clearToken();
      clearUserId();

      // Clear user in store
      var { dispatch } = this.props;
      dispatch(clearUser());

      // Redirect to login
      this.setState({ redirectToReferrer: true });

    }
    render() {
      // Check user in store
      if(this.props.store.user === null) return null;

      // Redirect if sign out
      if(this.state.redirectToReferrer) {
          return (
              <Redirect to="/login" />
          )
      }

      return (
        <div className="NavUser">
          <Paper zDepth={1} rounded={false} transitionEnabled={false}>
            <MenuItem
              className="MenuItem"
              primaryText={this.props.store.user.name}
              rightIcon={<i className="material-icons rightIcon">keyboard_arrow_down</i>}
              onTouchTap={this.handleTouchTap}
              disableTouchRipple={true}
              />
            <FloatingActionButton
              className="Icon-Add-Event"
              mini={true}
              secondary={true}
              onTouchTap={this.handleOpenModalCreateEvent}>
              <ContentAdd />
            </FloatingActionButton>
          </Paper>
          <Popover
            className="NavUser-Popover"
            open={this.state.isOpenPopover}
            anchorEl={this.state.anchorEl}
            // animated={false}
            useLayerForClickAway={false}
            onRequestClose={this.handleRequestClose}
            >
            <Menu className="NavUser-Popover-Menu" onItemTouchTap={this.handleRequestClose}>
              <MenuItem disableTouchRipple={true} className="NavUser-Popover-MenuItem" primaryText="Profile & account" onTouchTap={this.handleOpenNavRightProfile}/>
              <MenuItem disableTouchRipple={true} className="NavUser-Popover-MenuItem" primaryText="My list events" onTouchTap={this.handleOpenModalMyEvents}/>
              <MenuItem disableTouchRipple={true} className="NavUser-Popover-MenuItem" primaryText="Settings" />
              <MenuItem disableTouchRipple={true} className="NavUser-Popover-MenuItem" primaryText="Sign out" onTouchTap={this.handleSignOut}/>
            </Menu>
          </Popover>
        </div>
      )
    }
}

export default connect((state) => {
  return {
    store: {
      user: state.user
    }
  }
})(NavUser);
