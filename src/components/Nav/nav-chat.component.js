import React, {
    Component
} from 'react';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import ChatAction from '../Chat/chat-action.component';
import ChatBoard from '../Chat/chat-board.component';

class ChatNav extends Component {
  render() {
    if(this.props.store.user === null || this.props.store.currentEvent === null)
      return null;

    return (
      <div className="ChatNav">
        <AppBar
          className="AppBar"
          title="Live chat"
          showMenuIconButton={false}
          iconElementRight={<IconButton className="Close-Button"><NavigationClose /></IconButton>}
          onRightIconButtonTouchTap={this.props.toggleRightNav}
        />
        <Divider />
        <ChatBoard
          messages = { this.props.store.currentEvent.messages }
          ownerId = { this.props.store.currentEvent.owner.id }
          userId = { this.props.store.user._id } />
        <Divider />
        <ChatAction
          userId = { this.props.store.user._id }
          eventId = { this.props.store.currentEvent._id }
          name = { this.props.store.user.name }
          isAdmin = { this.props.store.user.isAdmin }
          avatar = { this.props.store.user.avatar } />
      </div>
    )
  }
}

export default connect((state) => {
  return {
    store: {
      user: state.user,
      currentEvent: state.currentEvent
    }
  }
})(ChatNav);
