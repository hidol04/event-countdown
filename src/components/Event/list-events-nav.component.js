import React, {
    Component
} from 'react';
import EventDetailNav from './event-detail-nav.component';
import { connect } from 'react-redux';
import { isOpenCreateEventModal, getMyEvents } from '../../redux/action';
import Menu from 'material-ui/Menu';
import { getUserId } from '../../blocks/session/session';
import { getEventsByUser } from '../../blocks/api/event.api';

class ListEventsNav extends Component {
    constructor(props) {
      super(props);
      this.state = {
          flag: false,
          events: [],
      };
      this.getEventsByUser = this.getEventsByUser.bind(this);
      this.showCreateEventModal = this.showCreateEventModal.bind(this);
      this.getEventsByUser();
    }

    getEventsByUser() {
      let params = {
          user_id: getUserId(),
      }

      // Call to get events by user api
      getEventsByUser(params).then((res) => {
        // Update state
        this.setState({
          flag: true,
          events: res.body,
        });
        if (!res.body.success) {
          // Get events failed
        } else {
          // Update store: myEvents
          var { dispatch } = this.props;
          dispatch(getMyEvents(res.body.success.data));
        }
      });
    }
    showCreateEventModal() {
        var {dispatch} = this.props;
        dispatch(isOpenCreateEventModal(true));
    }
    render() {
      if (!this.state.flag || this.props.store.myEvents === null) return null;

      return(
        <Menu>
          {this.props.store.myEvents.map(function(event, i) {
            return (
              <div key={i}>
                <EventDetailNav>{event}</EventDetailNav>
              </div>
            )
          })}
        </Menu>
      )
    }
}

export default connect((state) => {
  return {
    store: {
      myEvents: state.myEvents,
    }
  }
})(ListEventsNav);
