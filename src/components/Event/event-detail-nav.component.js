import React, {
    Component
} from 'react';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class EventDetailNav extends Component {
  handleShowName(name) {
    if(name.length > 25) {
      return name.substring(0, 25) + "...";
    }

    return name;
  }
  render() {
    let isActive = (this.props.store.currentEvent && this.props.children._id === this.props.store.currentEvent._id) ? true : false;
    if(isActive) {
      return(
        <div className="EventDetailNav">
          <MenuItem
            className="active"
            containerElement={<Link to={`/countdown/launch?eventId=${this.props.children._id}`} />}
            primaryText={this.handleShowName(this.props.children.name)}
            rightIcon={<i className="material-icons">keyboard_arrow_right</i>}
            />
        </div>
      )
    } else {
      return(
        <div className="EventDetailNav">
          <MenuItem
            containerElement={<Link to={`/countdown/launch?eventId=${this.props.children._id}`} />}
            primaryText={this.handleShowName(this.props.children.name)}
            />
        </div>
      )
    }
  }
}

export default connect(function(state) {
    return {
      store: {
        currentEvent: state.currentEvent
      }
    }
})(EventDetailNav);
