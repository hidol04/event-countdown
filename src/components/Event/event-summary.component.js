import React, {
  Component
} from 'react';
import { connect} from 'react-redux';
import { toggleAppShow } from '../../redux/action';
import { Link } from 'react-router-dom';
import Config from '../../config';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';

class EventSummary extends Component {
  constructor(props) {
    super(props);

    this.openAppShow = this.openAppShow.bind(this);
  }
  openAppShow() {
    var { dispatch } = this.props;
    dispatch(toggleAppShow(true));
  }
  handleBrokenImage(e) {
    e.target.src = Config.BASE_URL_SERVER_EVENT + "default.png";
    return true;
  }
  render() {
    if(this.props.data === null)
      return null;

    const event = {
      id: this.props.data._id,
      name: this.props.data.name,
      owner: this.props.data.owner.name,
      location: this.props.data.location,
      cover: this.props.data.cover,
      date_end_month: (new Date(this.props.data.date_end)).getMonth() + 1,
      date_end_day: (new Date(this.props.data.date_end)).getDate(),
      goingCount: this.props.data.going.length,
      interestedCount: this.props.data.interested.length,
    }

    return (
      <div className="Event-Detail Event-Summary">
        <Paper className="Event-Detail-Paper" zDepth={1} rounded={false} transitionEnabled={false}>
          <div className="Event-Cover">
            <Link to={`/countdown/launch?eventId=${event.id}`}>
              <img className="EventCover" width="100%" src={Config.BASE_URL_SERVER_EVENT + event.cover + ".png"} alt="" onError={this.handleBrokenImage}/>
            </Link>
          </div>
          <div className="Event-Header">
            <div className="Event-Header-Time">
              <span className="month">Tháng {event.date_end_month}</span><br />
              <span className="day">{event.date_end_day}</span>
            </div>
            <div className="Event-Header-Title">
              <span className="title">{event.name}</span><br />
              <span className="small-title">
                Công khai ·
                Tổ chức bởi: {event.owner}
              </span>
            </div>
            <div className="Event-Header-Going">
              <span className="name">Going</span><br />
              <span className="total">{event.goingCount}</span>
            </div>
            <div className="Event-Header-Interested">
            <span className="name">Interested</span><br />
            <span className="total">{event.interestedCount}</span>
            </div>
          </div>
          <Divider />
        </Paper>
      </div>
    )
  }
}

export default connect()(EventSummary);
