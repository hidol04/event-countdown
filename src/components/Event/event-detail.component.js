import React, {
  Component
} from 'react';
import { connect} from 'react-redux';
import { toggleAppShow } from '../../redux/action';
import Config from '../../config';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import { handleIncrease, handleDecrease, updateCover } from '../../blocks/api/event.api';
import { handleIncreaseOnEvent, handleDecreaseOnEvent, handleIncreaseOnUser, handleDecreaseOnUser } from '../../redux/action';
import {GridList, GridTile} from 'material-ui/GridList';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import Subheader from 'material-ui/Subheader';
import CircularLoader from '../Loader/circular.component';
import UploadPhotosEvent from '../Modal/upload-photo-event.modal';
import CONFIG from '../../config';
import Chip from 'material-ui/Chip';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreHorizIcon from 'material-ui/svg-icons/navigation/more-horiz';

class EventDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFirstLoad: true,
      isGoing: this.checkGI(this.props.user, this.props.event, 'going'),
      isInterested: this.checkGI(this.props.user, this.props.event, 'interested'),
      isOpenUploadPhotoModal: false,
      isLoadingGoing: false,
      isLoadingInterested: false
    }
    this.openAppShow = this.openAppShow.bind(this);
    this.handleGoing = this.handleGoing.bind(this);
    this.handleToggleUploadPhotoModal = this.handleToggleUploadPhotoModal.bind(this);
    this.handleInterested = this.handleInterested.bind(this);
    this.checkGI = this.checkGI.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      isGoing: this.checkGI(nextProps.user, nextProps.event, 'going'),
      isInterested: this.checkGI(nextProps.user, nextProps.event, 'interested')
    });
  }
  openAppShow() {
    var { dispatch } = this.props;
    dispatch(toggleAppShow(true));
  }
  handleGoing() {
    this.setState({ isLoadingGoing: true });

    var params = {
      type: 'going',
      userId: this.props.user._id,
      eventId: this.props.event._id
    };

    if (this.state.isGoing === true) {
      // Call Api
      handleDecrease(params).then((res) => {
        if(!res.body.success) return;

        // Update state in store: currentEvent && user
        let objE = {
          type: 'going',
          user_id: this.props.user._id
        }
        let objU = {
          type: 'going',
          event_id: this.props.event._id
        }
        var { dispatch } = this.props;
        dispatch(handleDecreaseOnEvent(objE));
        dispatch(handleDecreaseOnUser(objU));

        this.setState({
          isGoing: false,
          isLoadingGoing: false
        });
      });
    } else {
      // Call Api
      handleIncrease(params).then((res) => {
        if(!res.body.success) return;

        let going = res.body.success.data;

        // Update state in store: currentEvent
        let objE = {
          type: 'going',
          going: going
        }
        let objU = {
          type: 'going',
          going: {
            id: this.props.event._id,
            name: this.props.event.name,
            time: going.time
          }
        }

        var { dispatch } = this.props;
        dispatch(handleIncreaseOnEvent(objE));
        dispatch(handleIncreaseOnUser(objU));

        this.setState({
          isGoing: true,
          isLoadingGoing: false
        });
      });
    }
  }
  handleInterested() {
    this.setState({ isLoadingInterested: true });

    var params = {
      type: 'interested',
      userId: this.props.user._id,
      eventId: this.props.event._id
    };

    if (this.state.isInterested === true) {
      // Call Api
      handleDecrease(params).then((res) => {
        if(!res.body.success) return;

        // Update state in store: currentEvent && user
        let objE = {
          type: 'interested',
          user_id: this.props.user._id
        }
        let objU = {
          type: 'interested',
          event_id: this.props.event._id
        }
        var { dispatch } = this.props;
        dispatch(handleDecreaseOnEvent(objE));
        dispatch(handleDecreaseOnUser(objU));

        this.setState({
          isInterested: false,
          isLoadingInterested: false
        });
      });
    } else {
      // Call Api
      handleIncrease(params).then((res) => {
        if(!res.body.success) return;

        let interested = res.body.success.data;

        // Update state in store: currentEvent
        let objE = {
          type: 'interested',
          interested: interested
        }
        let objU = {
          type: 'interested',
          interested: {
            id: this.props.event._id,
            name: this.props.event.name,
            time: interested.time
          }
        }

        var { dispatch } = this.props;
        dispatch(handleIncreaseOnEvent(objE));
        dispatch(handleIncreaseOnUser(objU));

        this.setState({
          isInterested: true,
          isLoadingInterested: false
        });
      });
    }
  }
  handleToggleUploadPhotoModal(isOpen) {
    if(this.state.isOpenUploadPhotoModal === false) {
      this.setState({ isOpenUploadPhotoModal: true });
    }
    else {
      this.setState({ isOpenUploadPhotoModal: false });
    }
  }
  getAllPhotos() {
    let total = this.props.event.photos.length;
    let photos = [];
    // let limit = (total < 4) ? total : 4;
    let limit = total;
    let i;
    for(i = total - 1; i >= total - limit ; i-- ) {
      photos.push({
        img: this.props.event.photos[i].name,
        author: this.props.event.photos[i].uploader.name,
      });
    }

    if(photos[0])
      photos[0].featured = true;

    return photos;
  }
  checkGI(user, event, type) {
    // CHECK GI
    if(type === 'going'){
      let i = 0;
      for(i; i < user.going.length; i++) {
        if(user.going[i].id === event._id)
          return true;
      }
      return false;
    } else if(type === 'interested') {
      let i = 0;
      for(i; i < user.interested.length; i++) {
        if(user.interested[i].id === event._id)
          return true;
      }
      return false;
    }
  }
  componentDidMount() {
    // Set width
    let screenWeight = window.innerWidth;
    document.getElementById("EventDetail").style.marginLeft = ((screenWeight - 256 - 400 - 12) - 600)/2  + 'px';
    window.addEventListener("resize", function(event) {
      let w = window.innerWidth;
      document.getElementById("EventDetail").style.marginLeft = ((w - 256 - 400 - 12) - 600)/2  + 'px';
    })
  }
  handleTouchTapHashTag() {

  }
  handleFileChange(e) {
    var self = this;
    var reader = new FileReader();
    var file = e.target.files[0];

    reader.onload = function(upload) {
      self.setState({
        file: file,
      }, () => {
        // Create params to call API
        let params = new FormData();
        params.append("id", String(self.props.event._id));
        params.append("userId", self.props.user._id);
        params.append("update-cover", self.state.file);
        console.log(self.props.event._id);

        // Call API
        updateCover(params).then((res) => {
          if(res.body.success) {
            // Reload page
            window.location.reload();
          } else {
            // Show message erros
          }
        });
      });
    }

    reader.readAsDataURL(file);
  }
  handleUploadFile() {
    document.getElementById("input-file-cover").click();
  }
  handleBrokenImage(e) {
    e.target.src = Config.BASE_URL_SERVER_EVENT + "default.png";
    return true;
  }
  render() {
    // Style
    const styles = {
      root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
      },
      gridList: {
        width: 500,
        height: 450,
        overflowY: 'auto',
      },
    };

    // Get all photos
    const tilesData = this.getAllPhotos();
    //
    // let photosCount = tilesData.length;
    // let lastPhotoPreview = document.getElementById('Tile-2');
    // if(lastPhotoPreview) {
    //   let html = (photosCount <= 3) ? '' : "<div className='overlay'><span>+ 5</span</div>";
    //   lastPhotoPreview.innerHTML = lastPhotoPreview.innerHTML + html;
    // }

    const event = {
      name: this.props.event.name,
      owner: this.props.event.owner.name,
      location: this.props.event.location,
      hashtags: this.props.event.hashtags,
      cover: this.props.event.cover,
      date_start: new Date(this.props.event.date_start),
      date_end: new Date(this.props.event.date_end),
      date_end_month: (new Date(this.props.event.date_end)).getMonth() + 1,
      date_end_day: (new Date(this.props.event.date_end)).getDate(),
      going: this.props.event.going,
      interested: this.props.event.interested,
      description: this.props.event.description,
    }

    return (
      <div id="EventDetail" className="Event-Detail">
        <Paper className="Event-Detail-Paper" zDepth={1} rounded={false} transitionEnabled={false}>
          <div className="Event-Cover">
            <img id="EventCover" width="100%" src={Config.BASE_URL_SERVER_EVENT + event.cover + ".png"} alt="" onError={this.handleBrokenImage}/>
            <IconButton
              className="zoom_out"
              onTouchTap = {this.openAppShow}
              >
              <i className="material-icons">zoom_out_map</i>
            </IconButton>
            <IconMenu
              className="IconMenu"
              touchTapCloseDelay={1}
              iconButtonElement={<IconButton><MoreHorizIcon /></IconButton>}
              anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}
              animated={false}
            >
              <MenuItem onTouchTap={this.handleUploadFile} disableTouchRipple={true} className="NavListEvents-IconMenu-MenuItem" primaryText="Update Cover" />
            </IconMenu>
          </div>
          <div className="Event-Header">
            <div className="Event-Header-Time">
              <span className="month">Tháng {event.date_end_month}</span><br />
              <span className="day">{event.date_end_day}</span>
            </div>
            <div className="Event-Header-Title">
              <span className="title">{event.name}</span><br />
              <span className="small-title">
                Công khai ·
                Tổ chức bởi: {event.owner}
              </span>
            </div>
            <div className="Event-Header-Going">
              <span className="name">Going</span><br />
              <span className="total">{event.going.length}</span>
            </div>
            <div className="Event-Header-Interested">
              <span className="name">Interested</span><br />
              <span className="total">{event.interested.length}</span>
            </div>
          </div>
          <Divider />
          <div className="Event-Action">
            <div className="rate">
              <span>Rate: </span>
              <i className="material-icons rating_icon">star</i>
              <i className="material-icons rating_icon">star</i>
              <i className="material-icons rating_icon">star</i>
              <i className="material-icons rating_icon">star</i>
              <i className="material-icons rating_icon">star</i>
            </div>
            {(this.state.isLoadingGoing) ? <div className="Loading"><CircularLoader isShow={true}/></div> :
              <RaisedButton
                id="Going-Button"
                className={"Going-Button Event-Going-" + this.state.isGoing}
                label="Going"
                disableTouchRipple={true}
                onTouchTap={this.handleGoing}
                icon={(this.state.isGoing) ? <i className="material-icons">done</i> : null}
              />
            }
            {(this.state.isLoadingInterested) ? <div className="Loading"><CircularLoader isShow={true}/></div> :
              <RaisedButton
                id="Interested-Button"
                className={"Event-Interested-" + this.state.isInterested}
                label="Interested"
                disableTouchRipple={true}
                onTouchTap={this.handleInterested}
                icon={(this.state.isInterested) ? <i className="material-icons">done</i>: null}
              />
            }
          </div>
          <Divider />
          <div className="Event-Infomation">
            <div className="Info">
              <table>
                <tbody>
                  <tr>
                    <td>Time Start</td>
                    <td>
                      <span className="date">
                        {event.date_start.getFullYear() + " / " +
                        (event.date_start.getMonth() + 1) + " / " +
                        event.date_start.getDate()}</span>
                      <span className="time">
                        {event.date_start.getHours() + ":" +
                        event.date_start.getMinutes()}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td>Time End</td>
                    <td>
                      <span className="date">
                        {event.date_end.getFullYear() + " / " +
                        (event.date_end.getMonth() + 1) + " / " +
                        event.date_end.getDate()}</span>
                      <span className="time">
                        {event.date_end.getHours() + ":" +
                        event.date_end.getMinutes()}
                      </span>
                    </td>
                  </tr>
                  {(event.location) ?
                  <tr>
                    <td>Location </td>
                    <td>{event.location}</td>
                  </tr> : null}
                  {(event.hashtags) ?
                    <tr>
                      <td>Hashtag</td>
                      <td className="HashtagTd">
                        {event.hashtags.map((ht, i) => {
                          return(
                            <Chip
                              className="Chip"
                              key={i}
                              onTouchTap={this.handleTouchTapHashTag}>
                              {ht}
                            </Chip>
                          )
                        })}
                      </td>
                    </tr> : null}
                </tbody>
              </table>
            </div>
            <div className="Map">

            </div>
          </div>
          <Divider />
          <div className="Event-Photos" style={styles.root}>
            <Subheader>
              Event images
              <IconButton
                className="Add-Photo"
                tooltip="Add new photos"
                tooltipPosition="bottom-left"
                tooltipStyles={{marginRight: '32px', marginTop: '-36px'}}
                onTouchTap={this.handleToggleUploadPhotoModal}
                keyboardFocused={true}
              >
                <i className="material-icons">add_a_photo</i>
              </IconButton>
            </Subheader>
            <GridList
              className="Event-Photos-List"
              cols={2}
              cellHeight={200}
              padding={1}
              style={styles.gridList}
            >
              {tilesData.map((tile, key) => (
                <GridTile
                  id={`Tile-` + key}
                  className="Event-Photos-Title"
                  key={tile.img}
                  title={tile.title}
                  actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
                  actionPosition="left"
                  titlePosition="top"
                  titleBackground="linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
                  cols={tile.featured ? 2 : 1}
                  rows={tile.featured ? 2 : 1}
                >
                  <img src={CONFIG.BASE_URL_SERVER_EVENT + tile.img + '.png'} alt="" />
                </GridTile>
              ))}
            </GridList>
          </div>
          <Divider />
          <div className="Event-Description" style={styles.root}>
            <Subheader>
              Description
            </Subheader>
            <div>{event.description}</div>
          </div>
        </Paper>

        <UploadPhotosEvent
          isOpen={this.state.isOpenUploadPhotoModal}
          toggle={this.handleToggleUploadPhotoModal}
          data={this.props.event}
          user={this.props.user}
        />

        <input id="input-file-cover" type="file" onChange={this.handleFileChange} style={{display: 'none'}}/>
      </div>
    )
  }
}

export default connect()(EventDetail);
