import React, {
    Component
} from 'react';
import EventDetailNav from './event-detail-nav.component';
import { connect } from 'react-redux';
import Menu from 'material-ui/Menu';

class ListEventsFollowNav extends Component {
  constructor(props) {
      super(props);
      this.state = {
          events: []
      };
      this.getGoingEvents = this.getGoingEvents.bind(this);
      this.getInterestedEvents = this.getInterestedEvents.bind(this);
      this.getAllFollowEvents = this.getAllFollowEvents.bind(this);
  }
  getGoingEvents() {
    let events = [];
    let goingEvents = this.props.store.user.going;
    // let myEvents = this.props.store.myEvents;
    for(let i = 0; i < goingEvents.length; i++) {
      events.push({
        _id: goingEvents[i].id,
        name: goingEvents[i].name
      });
    }

    return events;
  }
  getInterestedEvents() {
    let events = [];
    let interestedEvents = this.props.store.user.interested;
    for(let i = 0; i < interestedEvents.length; i++) {
      events.push({
        _id: interestedEvents[i].id,
        name: interestedEvents[i].name
      });
    }

    return events;
  }
  getAllFollowEvents() {
    let goingEvents = this.getGoingEvents();
    let interestedEvents = this.getInterestedEvents();
    let check = 0;
    if(goingEvents.length === 0) return interestedEvents;
    if(interestedEvents.length === 0) return goingEvents;

    for(let i = 0; i < goingEvents.length; i++) {
      for(let j = 0; j < interestedEvents.length; j++) {
        if(String(goingEvents[i]._id) !== String(interestedEvents[j]._id)) {
          check += 1;
        }
      }
      if(check === interestedEvents.length) {
        interestedEvents.push({
          _id: goingEvents[i]._id,
          name: goingEvents[i].name
        });
      }
      check = 0;
    }
    return interestedEvents;
  }
  componentDidMount() {
    // Set width
    let s = document.getElementById("list-events-follow-nav");
    // s.setAttribute('style', 'width: 280px !important');
  }
  render() {
      if (this.props.store.user === null ||
        this.props.store.myEvents === null) return null;
      let events = [];
      switch (this.props.filter) {
        case "All":
          events = this.getAllFollowEvents();
          break;
        case "Going":
          events = this.getGoingEvents();
          break;
        case "Interested":
          events = this.getInterestedEvents();
          break;
        default:
      }

      return(
        <div id="list-events-follow-nav">
          <Menu>
            {events.map(function(event, i) {
              return (
                <EventDetailNav key={i}>{event}</EventDetailNav>
              )
            })}
          </Menu>
        </div>
      )
  }
}

export default connect((state) => {
  return {
    store: {
      myEvents: state.myEvents,
      user: state.user
    }
  }
})(ListEventsFollowNav);
