import React, {
    Component
} from 'react';
import { connect} from 'react-redux';
import { Link } from 'react-router-dom';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import ActionHome from 'material-ui/svg-icons/action/home';

class ToolBar extends Component {
    render() {
      return (
        <Toolbar className="ToolBar">
          <ToolbarGroup firstChild={true}>
            <IconButton tooltip="Home">
              <Link to={`/`}>
                <ActionHome />
              </Link>
            </IconButton>
          </ToolbarGroup>
        </Toolbar>
      )
    }
}

export default connect()(ToolBar);
