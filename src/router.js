import React from 'react';
import Dashboard from './pages/dashboard';
import Login from './components/authentication/login.component';
import Register from './components/authentication/register.component';
import NoMatch from './pages/404_error/404_error';
import ProtectedRoute from './blocks/routes/protected-route';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

let routes = (
    <BrowserRouter>
        <Switch>
            <Route exact path = "/login" component = {Login} />
            <Route exact path = "/register" component = {Register} />
            <ProtectedRoute exact path = "/" component = {Dashboard} />
            <ProtectedRoute exact path = "/countdown/launch/" component = {Dashboard} />
            <Route exact component = {NoMatch} />
        </Switch>
    </BrowserRouter>
);

export default routes;
