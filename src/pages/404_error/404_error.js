import React, {
    Component
} from 'react';
import './404_error.css';

class NoMatch extends Component {
    render() {
        return (
            <div className="wrap">
                <div className="err_content">
                    <div className="logo">
                        <h1><a href=""><img src="../images/404_page/logo.png" alt="" /></a></h1>
                    </div>
                </div>
            </div>
        )
    }
}

export default NoMatch;
