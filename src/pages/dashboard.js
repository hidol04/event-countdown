import React, {
    Component
} from 'react';
import NavLeft from '../components/Nav/nav-left.component';
import NavRightProfile from '../components/Nav/nav-right-profile.component';
import AppDetail from '../components/App/app-detail.component';
import AppHome from '../components/App/app-home.component';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import UpdateProfileModal from '../components/Modal/update-profile.modal';
import CreateEventModal from '../components/Modal/CreateEvent/create-event.modal';
import ShowMyEvents from '../components/Modal/show-my-events.modal';
import ToolBar from '../components/Toolbar/toolbar.component';
import { connect} from 'react-redux';
import { changeCurrentEvent, changeUser } from '../redux/action';
import queryString from 'query-string';
import { getEventById } from '../blocks/api/event.api';
import { getUserById } from '../blocks/api/user.api';
import { getUserId } from '../blocks/session/session.js';

var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

class Dashboard extends Component {
    constructor(props) {
      console.log('constructor dashboard');
      super(props);

      this.state = {
        isHomePage: true,
        eventId: ''
      }

      this.updateStoreEvent = this.updateStoreEvent.bind(this);
    }
    componentWillMount() {
      // Update state in store: user
      let params = {
        id: getUserId()
      }

      getUserById(params).then((res) => {
        var { dispatch } = this.props;
        dispatch(changeUser(res.body.success.data));
      });
    }
    updateStoreEvent(eventId) {
      if(this.state.eventId === eventId) return ;

      let params = {
          id: eventId
      }

      getEventById(params)
      .then((res) => {
        if (res.body.success.data === null) return;

        // Update state in store: currentEvent
        var { dispatch } = this.props;
        dispatch(changeCurrentEvent(res.body.success.data));
        //
        this.setState({ eventId: res.body.success.data._id });
      });
    }
    render() {
      // Get value of param eventId in url
      let parsed = queryString.parse(this.props.location.search);
      // Update state in store: currentEvent
      if(parsed.eventId)
        this.updateStoreEvent(parsed.eventId);

      return (
        <MuiThemeProvider>
          <div id="dashboard" className="row">
            <NavLeft />
            <ToolBar />
            {(parsed.eventId) ? <AppDetail /> : <AppHome />}
            <CreateEventModal />
            <NavRightProfile />
            <UpdateProfileModal />
            <ShowMyEvents />
          </div>
        </MuiThemeProvider>
      )
    }
}

export default connect()(Dashboard);
