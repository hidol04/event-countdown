import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getToken } from '../session/session.js';

const ProtectedRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (

        // Check token exists
        getToken() ? (
            <Component {...props}/>
        ) : (
                <Redirect to = '/login' />
            )
    )} />
)

export default ProtectedRoute;
