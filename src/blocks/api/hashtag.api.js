import Request from 'superagent';
import Config from '../../config';

export function getHashtags(params) {
  let url = Config.BASE_URL_API + "hashtag/";
  // Call to api
  return Request.get(url, params);
}
