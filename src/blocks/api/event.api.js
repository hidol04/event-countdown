import Request from 'superagent';
import Config from '../../config';


export function getEventById(params) {
  let url = Config.BASE_URL_API + "event/getEventById";
  // Call to api
  return Request.post(url, params);
}

export function getEventsByUser(params) {
  let url = Config.BASE_URL_API + "events/getEventsByUser";
  // Call to api
  return Request.post(url, params);
}

export function createEvent(params) {
  let url = Config.BASE_URL_API + "event/create";
  // Call to api
  return Request.post(url, params);
}

export function getAllEvents() {
  let url = Config.BASE_URL_API + "events";
  // Call to api
  return Request.get(url);
}

// Increase `going` || `interested`
export function handleIncrease(params) {
  let url = Config.BASE_URL_API + "event/update/handleIncrease";
  // Call to
  return Request.put(url, params);
}

// Decrease `going` || `interested`
export function handleDecrease(params) {
  let url = Config.BASE_URL_API + "event/update/handleDecrease";
  // Call to api
  return Request.put(url, params);
}

// Update event
export function updateEvent(params) {
  let url = Config.BASE_URL_API + "event/update";

  // Call to api
  return Request.put(url, params);
}

// Update cover
export function updateCover(params) {
  let url = Config.BASE_URL_API + "event/updateCover";

  // Call to api
  return Request.put(url, params);
}

// Upload photos
export function uploadPhotosApi(params) {
  let url = Config.BASE_URL_API + "event/uploadPhotos";

  // Call to api
  return Request.put(url, params);
}
