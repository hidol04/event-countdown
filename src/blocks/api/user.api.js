import Request from 'superagent';
import Config from '../../config';

export function getUserById(params) {
    let url = Config.BASE_URL_API + "user/";
    // Call to api
    return Request.post(url, params);
}

export function updateUser(params) {
  console.log(params);
  let url = Config.BASE_URL_API + "user/update";
  // let url = "http://localhost:8080/user/update";

  // Call to api
  return Request.put(url, params);
}
