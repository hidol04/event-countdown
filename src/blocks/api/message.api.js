import Request from 'superagent';
import Config from '../../config';

export function sendMessage(params) {
    let url = Config.BASE_URL_API + "event/message/create/";
    // Call to api
    return Request.put(url, params);
}
