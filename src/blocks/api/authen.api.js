import Request from 'superagent';
import Config from '../../config';

const Authen = {
    login,
    register
}

function login(params) {
    let url = Config.BASE_URL_API + "authenticate/login";
    // Call to login's api
    return Request.post(url, params);
}

function register(params) {
    let url = Config.BASE_URL_API + "authenticate/register";
    // Call to register's api
    return Request.post(url, params);
}

export default Authen;
