const timezone = {
  '-12': '(GMT -12:00) Eniwetok, Kwajalein',
  '-11': '(GMT -11:00) Midway Island, Samoa',
  '-10': '(GMT -10:00) Hawaii',
  '-9': '(GMT -9:00) Alaska',
  '-8': '(GMT -8:00) Pacific Time (US & Canada)',
  '-7': '(GMT -7:00) Mountain Time (US &amp; Canada)',
  '-6': '(GMT -6:00) Central Time (US &amp; Canada), Mexico City',
  '-5': '(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima',
  '-4': '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz',
  '-3': '(GMT -3:00) Newfoundland',
  '-2': '(GMT -3:30) Brazil, Buenos Aires, Georgetown',
  '-1': '(GMT -2:00) Mid-Atlantic',
  '0': '(GMT -1:00) Azores, Cape Verde Islands',
  '+1': '(GMT) Western Europe Time, London, Lisbon, Casablanca',
  '+2': '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris',
  '+3': '(GMT -12:00) Kaliningrad, South Africa',
  '+4': '(GMT -12:00) Baghdad, Riyadh, Moscow, St. Petersburg',
  '+5': '(GMT -12:00) Kaliningrad, South Africa',
  '+6': '(GMT -12:00) Tehran',
  '+7': '(GMT -12:00) Abu Dhabi, Muscat, Baku, Tbilisi',
  '8': '(GMT -12:00) Kabul',
  '9': '(GMT -12:00) Baghdad, Riyadh, Moscow, St. Petersburg',
  '10': '(GMT -12:00) Tehran',
  '11': '(GMT -12:00) Abu Dhabi, Muscat, Baku, Tbilisi',
  '12': '(GMT -12:00) Kabul',
  '13': '(GMT -12:00) Ekaterinburg, Islamabad, Karachi, Tashkent',
  '14': '(GMT -12:00) Bombay, Calcutta, Madras, New Delhi',
  '15': '(GMT -12:00) Kathmandu',
  '16': '(GMT -12:00) Almaty, Dhaka, Colombo',
  '17': '(GMT -12:00) Bangkok, Hanoi, Jakarta',
  '+18': '(GMT -12:00) Bangkok, Hanoi, Jakarta',
  '+19': '(GMT -12:00) Beijing, Perth, Singapore, Hong Kong',
  '+20': '(GMT -12:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk',
  '+21': '(GMT -12:00) Adelaide, Darwin',
  '+22': '(GMT -12:00) Eastern Australia, Guam, Vladivostok',
  '+23': '(GMT -12:00) Magadan, Solomon Islands, New Caledonia',
  '+24': '(GMT -12:00) Auckland, Wellington, Fiji, Kamchatka',
}
