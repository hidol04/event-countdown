// Token
export function setToken(token) {
    localStorage.setItem('token', token);
}

export function getToken() {
    return localStorage.getItem('token');
}

export function clearToken() {
    return localStorage.removeItem('token');
}

// User Id
export function setUserId(id) {
    localStorage.setItem('user_id', id);
}

export function getUserId() {
    return localStorage.getItem('user_id');
}

export function clearUserId() {
    return localStorage.removeItem('user_id');
}
