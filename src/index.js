import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import Router from './router';
import store from './redux/store';
import { Provider } from 'react-redux';
import './index.css';
import './components/App/App.css';
import './components/authentication/authen.css';
import './components/Chat/chat.css';
import './components/Event/event.css';
import './components/Modal/modal.css';
import './components/Nav/nav.css';
import './components/Toolbar/toolbar.css';

ReactDOM.render(
  <Provider store={store}>
    {Router}
  </Provider>, document.getElementById('root'));

registerServiceWorker();
