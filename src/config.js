const config = {
  // Localhost
    // BASE_URL_API : 'http://localhost:8080/api/v1/',
    // BASE_URL_SERVER_AVATAR : 'http://localhost:8080/avatars/',
    // BASE_URL_SERVER_EVENT : 'http://localhost:8080/events/',
  // Heroku
    BASE_URL_API : 'https://event-countdown.herokuapp.com/api/v1/',
    BASE_URL_SERVER_AVATAR : 'https://event-countdown.herokuapp.com/avatars/',
    BASE_URL_SERVER_EVENT : 'https://event-countdown.herokuapp.com/events/',
    // Socket
    URL_SOCKET : 'http://event-countdown-socket.herokuapp.com/'
}

export default config;
